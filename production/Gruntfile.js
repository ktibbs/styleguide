module.exports = function (grunt) {
require('load-grunt-tasks')(grunt);
 
  grunt.initConfig({
    
bake: {
        build: {
            files: {
                "../index.html": "core.html",
                "../organisms/article-block.html": "organisms/article-block.html",
                "../organisms/article-with-list.html": "organisms/article-with-list.html",
                "../organisms/teaser-list.html": "organisms/teaser-list.html",
                "../templates/article-page.html": "templates/article-page.html"

            },
        },
    },
 
 copy: {
  main: {
    files: [
      {expand: true, src: ['css/**','js/**','img/**','404.html'], dest: '../'},
    ],
  },
},

});

  // Default task(s).
  grunt.registerTask('default', ['cssmin']);
  grunt.registerTask('html', ['bake']);
  grunt.registerTask('move', ['copy'])
};
