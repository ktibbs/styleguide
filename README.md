# README #

Styleguide built using angular, the aim is to create a single page app that acts as a styleguide, which can be integrated into any site.

Advantages of using angular. Can create custom templates very quickly, and therefore provide prototypes to design/PO as to how a page will look using current theme.

Also, as a bonus, as its a single page app, making changes to the styles on one page in firebug wont be blown away when navigating to another page