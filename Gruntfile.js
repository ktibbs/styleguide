module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
      compass: {                  // Task
        dev: {                   // Target
          options: {
            config: 'config.rb',
          }
        },
      },
      watch: {
        options: {
        },
        sass: {
          files: ['scss/{,**/}*.{scss,sass}'],
          tasks: ['compass:dev', 'autoprefixer'],
          options: {
          }
        },

        images: {
          files: ['images/**']
        },

        css: {
          files: ['css/{,**/}*.css']
        },

        js: {
          files: ['js/{,**/}*.js', '!js/{,**/}*.min.js'],
          tasks: ['jshint', 'uglify:dev']
        }
      },


      autoprefixer: {
        options: {
          browsers: ['last 2 versions']
        },
      }
});

  // Default task(s).
  grunt.registerTask('default', ['autoprefixer']);
  grunt.registerTask('runit', ['watch']);
};
