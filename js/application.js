var styleApp = angular.module('styleApp', ['ngRoute', 'ngAnimate']);


styleApp.controller('navCtrl', function ($scope) {

$scope.atomsList = [
        {'url': '#styles', 
            'text': 'Base Styles'
        },
        {   'url': '#forms', 
            'text': 'Form styling'
        },
        {   'url': '#buttons', 
            'text': 'Buttons'
        },
         ];    

$scope.navList = [
        {'url': '#article-block', 
        	'text': 'Article block'
        },
        {	'url': '#article-with-list', 
        	'text': 'Article and list'
        },
        {   'url': '#teaser-list', 
            'text': 'Teaser list'
        },
        {   'url': '#comment-form', 
            'text': 'Comment form'
        },
        {   'url': '#images', 
            'text': 'Images'
        },
         ];    
$scope.templateList = [
        {'url': '#article-page', 
        'text': 'Article page'
        },
        {'url': '#article-page-comments', 
        'text': 'Article page with comments'
        }
         ];  

    //$scope.openSide = true;
         
});
/*styleApp.controller('mainCtrl', function ($scope) {
$scope.clicky = function() {
    $scope.openSide = true;
console.log('Hello');
    };
    });*/
styleApp.controller('mainCtrl', function ($scope) {
$scope.class_status = 0;
$scope.toggleSingleClass = function() {  //toggle the sidebar. yay it works
    $scope.class_status = !$scope.class_status;
    $scope.single_message ="class is toggle";
  };

$scope.removeSingleClass = function() {  //yay it works
    $scope.class_status = 0;

    //console.log('Hello');
    };

});

// create the module and name it scotchApp
        // also include ngRoute for all our routing needs
    // configure our routes
    styleApp.config(function($routeProvider) {
        $routeProvider
            // route for the home page

               .when('/', {
                templateUrl : 'atoms/styles.html',
                //controller  : 'mainCtrl'

            })
                .when('/styles', {
                templateUrl : 'atoms/styles.html',
                //controller  : 'mainCtrl'

            })

            .when('/forms', {
                templateUrl : 'atoms/form.html',
                //controller  : 'mainCtrl'

            })

            .when('/buttons', {
                templateUrl : 'atoms/buttons.html',
                //controller  : 'mainCtrl'

            })

            .when('/home', {
                templateUrl : 'pages/styles.html',
                //controller  : 'mainCtrl'

            })
            // route for the article-block page
            .when('/article-block', {
                templateUrl : 'organisms/article-block.html',
                //controller  : 'mainCtrl'
            })

            // route for the article with list page
            .when('/article-with-list', {
                templateUrl : 'organisms/article-with-list.html',
                //controller  : 'bblockController'

            })
            .when('/teaser-list', {
                templateUrl : 'organisms/teaser-list.html',
                //controller  : 'mainCtrl'
            })
            .when('/comment-form', {
                templateUrl : 'organisms/comment-form.html',
                //controller  : 'mainCtrl'
            })
            .when('/images', {
                templateUrl : 'organisms/images.html',
                //controller  : 'mainCtrl'
            })
            .when('/article-page', {
                templateUrl : 'templates/article-page.html',
                //controller  : 'mainCtrl'
            })
            .when('/article-page-comments', {
                templateUrl : 'templates/article-page-comment-form.html',
                //controller  : 'mainCtrl'
            });

            // use the HTML5 History API
        //$locationProvider.html5Mode(true);
        
    });



